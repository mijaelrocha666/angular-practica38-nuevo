import { Component, OnInit } from '@angular/core';
import { IPersona } from 'src/app/persona-interface/persona.interface';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  persona: IPersona = {
    nombre: '',
    direccion: '',
    aceptar: ''
  }

  constructor() { }

  ngOnInit(): void {
  }

  Ir(): void {
    console.log(this.persona.nombre);
    console.log(this.persona.direccion);
    console.log(this.persona.aceptar);
    
  }

}
