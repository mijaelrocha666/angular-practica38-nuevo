export interface IPersona {
  nombre: string;
  direccion: string;
  aceptar: string;
}